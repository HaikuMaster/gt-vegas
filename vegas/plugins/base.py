import asyncio
import discord

from discord.ext import commands
from functools import partial
"""
BasePlugin

Used as a plugin base for all VEGAS Plugins
"""

TIMEOUT = 30.0

class BasePlugin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def _get_guild(self, context):
        """Private method that returns a guild

        This method will first check the provided `context.guild` for a `discord.Guild` object. If
        one cannot be found (such as the user is in a DMChannel), the user will be prompted to
        provide a `discord.Guild.id`. The object is then lookedup and returned.

        Parameters
        ----------
        context: discord.Context
            A discordpy Context object.

        Returns
        -------
            guild: discord.Guild if successful or None
                If the user provides a `discord.Guild.id` to a guild the bot is in, the Guild is returned,
                otherwise None is returned.

        """
        if context.guild:
            return context.guild

        await context.send("Please send a Server ID:")

        check_is_dm_and_author_message = partial(self._check_is_dm_and_author_message_partial, context.message.author)

        while True:
            try:
                message = await self.bot.wait_for('message', timeout=TIMEOUT, check=check_is_dm_and_author_message)
                if message.content.lower() == 'cancel':
                    await context.send("Thanks for using {}".format(self.bot.user.name))
                    break
                guild = self.bot.get_guild(int(message.content))
                if not guild:
                    await context.send("I am not in a guild with that ID.")
                return guild
            except asyncio.TimeoutError as e:
                await context.send("If you'd like to cancel, reply with `Cancel`")
            except ValueError as e:
                await context.send("I need a Server ID (a number) to lookup, or reply with `Cancel` to quit.")

    def _check_is_dm_and_author_message_partial(self, member, message):
        """Ensures the Caller is the Mod Contact author and is in a DMChannel"""
        return member == message.author and isinstance(message.channel, discord.DMChannel)

    async def _check_user_exists(self, user_id, guild_id):
        """Private method to ensure a users discord_user_discorduser record exists.

        This method accepts a users id and a guild id. The SQL that gets executed
        will first check to see if a record for this user/guild exists. If the
        record exsists, that id is returned. If no record exists, a new record is
        created with the user/guild id combo and that new id is returned.

        Parameters
        ----------
        user_id: int
            The discord.Member.id of a guild member.
        guild_id: int
            A discord.Guild.id.

        Returns
        -------
        link_id: int
            The id of the users discord_user_discorduser record
        """
        sql_insert = """with s as (
                            select id
                            from discord_user_discorduser
                            where discord_guild_id = '%s' and discord_user_id = '%s'
                        ), i as (
                            insert into discord_user_discorduser ("discord_guild_id", "discord_user_id")
                            select '%s', '%s'
                            where not exists (select 1 from s)
                            returning id
                        )
                        select id
                            from i
                        union all
                        select id
                            from s"""
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(sql_insert, (guild_id, user_id, guild_id, user_id))
                link_id = await cursor.fetchone()
                return link_id[0]
